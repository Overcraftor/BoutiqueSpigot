package fr.overcraftor.boutique.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import fr.overcraftor.boutique.inventories.BoutiqueInventory;
import fr.overcraftor.boutique.inventories.PurshaseConfirmInventory;

public class OnInventoryClick implements Listener{

    @EventHandler
    public void onClick(InventoryClickEvent e){
        if(e.getCurrentItem() == null) return;

        if(e.getView().getTitle().startsWith(BoutiqueInventory.invName))
            BoutiqueInventory.onClick(e);

        if(e.getView().getTitle().equals(PurshaseConfirmInventory.invName))
            PurshaseConfirmInventory.onClick(e);
    }
    
}
