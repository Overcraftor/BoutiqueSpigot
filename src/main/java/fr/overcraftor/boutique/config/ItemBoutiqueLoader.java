package fr.overcraftor.boutique.config;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import fr.overcraftor.boutique.Main;
import fr.overcraftor.boutique.utils.ItemBuilder;

public class ItemBoutiqueLoader {

    private final Material material;
    private final String name;
    private final List<String> lore;
    private final List<String> commands;
    private final int slot;
    private final int price;

    private final ItemStack item;

    public ItemBoutiqueLoader(String path, YamlConfiguration config) throws Exception {
        this.material = Material.getMaterial(config.getString(path + ".Material"));
        this.name = config.getString(path + ".Name");
        this.lore = config.getStringList(path + ".Lore");
        this.commands = config.getStringList(path + ".Commands");
        this.slot = config.getInt(path + ".Slot");
        this.price = config.getInt(path + ".Prix");

        if(material == null || name == null || lore == null || commands == null ){
            Main.getInstance().getLogger().severe("L'item " + path + " ne s'est pas correctement charge.");
            throw new Exception("Le fichier plugins/Boutique/boutique.yml est mal configure.");
        }

        this.item = new ItemBuilder(material, 1, name).setLore(lore).addFlag(ItemFlag.HIDE_ATTRIBUTES).toItemStack();
    }

    public int getSlot() {
        return slot;
    }

    public ItemStack getItem() {
        return item;
    }
    
    public int getPrice() {
        return price;
    }

    private String getName() {
        return name;
    }

    public static ItemBoutiqueLoader getByName(String name){
        return Main.getInstance().getItemsBoutique().stream().filter(item -> item.getName().equals(name)).findFirst().get();
    }

    public void executeCommands(String pName){
        commands.forEach(cmd ->{
            Main.getInstance().getServer().dispatchCommand(Bukkit.getConsoleSender(), 
                cmd.replace("{user}", pName));
        });
    }
}
