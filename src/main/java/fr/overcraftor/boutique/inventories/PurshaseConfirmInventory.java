package fr.overcraftor.boutique.inventories;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.overcraftor.boutique.Main;
import fr.overcraftor.boutique.config.ItemBoutiqueLoader;
import fr.overcraftor.boutique.utils.ItemBuilder;

public class PurshaseConfirmInventory {

    public static String invName = "§c§lConfirme ton achat";

    public static void openInventory(Player p, ItemBoutiqueLoader item, float money){
        Inventory inv = Bukkit.createInventory(null, 9 * 6, invName);

        final ItemStack confirm = new ItemBuilder(Material.EMERALD_BLOCK, "§a§lConfirmer").setLore("§cVos PB: §e" + money, "§cPrix: §e" + item.getPrice()).toItemStack();
        final ItemStack refuse = new ItemBuilder(Material.REDSTONE_BLOCK, "§c§lRefuser").toItemStack();
        final ItemStack glassPane = new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE, " ").toItemStack();

        int i = 0;
        while(i != 54){

            if((i >= 19 && i < 19 + 3) || (i >= 28 && i < 28 + 3) || (i >= 37 && i < 37 + 3))
                inv.setItem(i, confirm);
            else if((i >= 23 && i < 23 + 3) || (i >= 32 && i < 32 + 3) || (i >= 41 && i < 41 + 3))
                inv.setItem(i, refuse);
            else
                inv.setItem(i, glassPane);

            i++;
        }
        inv.setItem(4, item.getItem());

        p.closeInventory();
        p.openInventory(inv);
    }

    public static void onClick(InventoryClickEvent e){
        e.setCancelled(true);
        final Player p = (Player) e.getWhoClicked();
        final ItemStack current = e.getCurrentItem();

        if(current.getType().equals(Material.EMERALD_BLOCK)){

            final float money = Main.getInstance().getBoutiqueSQLManager().getPlayerMoney(p.getName());
            final ItemBoutiqueLoader item = ItemBoutiqueLoader.getByName(e.getInventory().getItem(4).getItemMeta().getDisplayName());

            if(item.getPrice() > money){
                p.sendMessage("§cIl vous manque §e" + (item.getPrice() - money) + "PB");
                p.closeInventory();
                return;
            }

            Main.getInstance().getBoutiqueSQLManager().removeMoney(p.getName(), item.getPrice());
            item.executeCommands(p.getName());
            p.closeInventory();

        }else if(current.getType().equals(Material.REDSTONE_BLOCK)){

            p.closeInventory();
            BoutiqueInventory.openInventory(p);
        }
    }
    
}
