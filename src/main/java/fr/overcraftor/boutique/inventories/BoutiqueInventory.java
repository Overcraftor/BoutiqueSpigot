package fr.overcraftor.boutique.inventories;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import fr.overcraftor.boutique.Main;
import fr.overcraftor.boutique.config.ItemBoutiqueLoader;

public class BoutiqueInventory {
    
    public static final String invName = "§cBoutique §7- §cPB: §e";

    public static void openInventory(Player p){
        final float money = Main.getInstance().getBoutiqueSQLManager().getPlayerMoney(p.getName());

        Inventory inv = Bukkit.createInventory(null, 9 * 6, invName + money);
        
        Main.getInstance().getItemsBoutique().forEach(item ->{
            inv.setItem(item.getSlot(), item.getItem());
        });

        p.openInventory(inv);
    }

    public static void onClick(InventoryClickEvent e){
        e.setCancelled(true);
        final Player p = (Player) e.getWhoClicked();
        final ItemBoutiqueLoader item = ItemBoutiqueLoader.getByName(e.getCurrentItem().getItemMeta().getDisplayName());
        final float money = Main.getInstance().getBoutiqueSQLManager().getPlayerMoney(p.getName());

        if(item.getPrice() > money){
            p.sendMessage("§cIl vous manque §e" + (item.getPrice() - money) + "PB");
            return;
        }

        PurshaseConfirmInventory.openInventory(p, item, money);
    }

}
