package fr.overcraftor.boutique.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.overcraftor.boutique.Main;
import fr.overcraftor.boutique.config.ConfigurationAPI;

public class BoutiqueSQLManager {

    private final Main main;
    private SQLConnection sql;
    private final ConfigurationAPI config;

    private final String tableName;
    private final String columnPlayerName;
    private final String columnMoneyName;

    public BoutiqueSQLManager(Main main){
        this.main = main;
        this.config = new ConfigurationAPI("BddBoutique.yml", main);
        
        this.tableName = config.get().getString("tableName");
        this.columnPlayerName = config.get().getString("columnPlayerName");
        this.columnMoneyName = config.get().getString("columnMoneyName");
    }

    public boolean mysqlConnection(){
        try{
            config.create();
    
            final String urlbase = config.get().getString("urlbase");
            final String host = config.get().getString("host");
            final String database = config.get().getString("database");
            final String user = config.get().getString("user");
            final String pass = config.get().getString("pass");

            this.sql = new SQLConnection(urlbase, host, database, user, pass);
            this.sql.connect();

        }catch(SQLException e){
            main.getLogger().warning("La connection mysql a echoue, veuillez bien configurer le fichier plugins/Boutique/BddBoutique.yml");
            return false;
        }

        try{
            PreparedStatement sts = sql.getConnection().prepareStatement("SELECT * FROM " + tableName + " WHERE " + columnMoneyName + " = ? AND " + columnPlayerName + " = ?");
            sts.setFloat(1, 5);
            sts.setString(2, "pseudo");
            sts.executeQuery();
            
            return true;
        }catch(Exception e){
            main.getLogger().warning("La table " + tableName + " et les colonnes " + columnPlayerName + ", " + columnMoneyName + " n'existent pas ,veuillez bien configurer le fichier plugins/Boutique/BddBoutique.yml");
        }

        return false;
    }

    public float getPlayerMoney(String p){
        try{
            PreparedStatement sts = sql.getConnection().prepareStatement("SELECT * FROM " + tableName + " WHERE " + columnPlayerName + " = ?");
            sts.setString(1, p);
            ResultSet rs = sts.executeQuery();

            if(rs.next())
                return rs.getFloat(columnMoneyName);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return 0;
    }

    public void removeMoney(String p, float money){
        try{
            PreparedStatement sts = sql.getConnection().prepareStatement("UPDATE " + tableName + " SET " + columnMoneyName + " = " + columnMoneyName + " - ? WHERE " + columnPlayerName + " = ?");
            sts.setFloat(1, money);
            sts.setString(2, p);
            sts.execute();
            sts.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public SQLConnection getSql() {
        return sql;
    }
}
