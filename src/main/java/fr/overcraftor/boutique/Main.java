package fr.overcraftor.boutique;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import fr.overcraftor.boutique.commands.BoutiqueCommand;
import fr.overcraftor.boutique.config.ConfigurationAPI;
import fr.overcraftor.boutique.config.ItemBoutiqueLoader;
import fr.overcraftor.boutique.events.OnInventoryClick;
import fr.overcraftor.boutique.mysql.BoutiqueSQLManager;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin{

    private static Main instance;
    private BoutiqueSQLManager boutiqueSQLManager;
    private ConfigurationAPI boutiqueConfig;

    private List<ItemBoutiqueLoader> itemsBoutique;

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "==============================================================");
        getLogger().info("Lancemement du plugin...");

        instance = this;
        this.boutiqueSQLManager = new BoutiqueSQLManager(this);
        this.boutiqueConfig = new ConfigurationAPI("boutique.yml", this);
        boutiqueConfig.create();
        this.itemsBoutique = new ArrayList<>();

        try{
            for(String path : boutiqueConfig.get().getKeys(false)){
                itemsBoutique.add(new ItemBoutiqueLoader(path, boutiqueConfig.get()));
            }
        }catch(Exception e){
            Bukkit.getConsoleSender().sendMessage("[" + getName() + "] " + ChatColor.RED + "Le plugin a ete desactive car le fichier plugins/Boutique/boutique.yml est mal configure.");
            Bukkit.getPluginManager().disablePlugin(this);
        }

        registration();

        //MYSQL
        if(boutiqueSQLManager.mysqlConnection()){
            
        }else{
            Bukkit.getConsoleSender().sendMessage("[" + getName() + "] " + ChatColor.RED + "Le plugin a ete desactive car la connection sql n'a pas pu etre correctement etablie");
            Bukkit.getPluginManager().disablePlugin(this);
        }
        Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "==============================================================");
    }

    @Override
    public void onDisable() {
        if(boutiqueSQLManager.getSql() != null)
            boutiqueSQLManager.getSql().disconnect();
    }

    private void registration(){
        getCommand("boutique").setExecutor(new BoutiqueCommand());

        Bukkit.getPluginManager().registerEvents(new OnInventoryClick(), this);
    }

    public static Main getInstance() {
        return instance;
    }

    public BoutiqueSQLManager getBoutiqueSQLManager() {
        return boutiqueSQLManager;
    }

    public List<ItemBoutiqueLoader> getItemsBoutique() {
        return itemsBoutique;
    }
}