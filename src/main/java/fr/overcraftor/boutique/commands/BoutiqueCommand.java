package fr.overcraftor.boutique.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.overcraftor.boutique.inventories.BoutiqueInventory;

public class BoutiqueCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        
        if(sender instanceof Player)
            BoutiqueInventory.openInventory((Player) sender);

        return false;
    }

}
